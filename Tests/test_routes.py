import unittest
from flask_server import node


class TestRouteAPI(unittest.TestCase):

    def setUp(self) -> None:
        self.tester = node.test_client(self)
        self.return_code_200 = 200

    def test_txion_route_should_return_a_200_response_on_POST_request(self):
        # GIVEN
        expected_return_code = 200
        # WHEN
        response = self.tester.post('/txion',
                                    data={
                                        "from": "akjflw",
                                        "to": "fjlakdj",
                                        "amount": 3
                                    },
                                    content_type='html/text')
        # THEN
        self.assertEqual(self.return_code_200, response.status_code)

    def test_blocks_route_should_return_a_200_response_on_GET_request(self):
        # GIVEN
        expected_return_code = 200
        # WHEN
        response = self.tester.get('/blocks', content_type='html/text')
        # THEN
        self.assertEqual(self.return_code_200, response.status_code)

    def test_mine_route_should_return_a_200_response_on_GET_request(self):
        # GIVEN
        expected_return_code = 200
        # WHEN
        response = self.tester.get('/mine', content_type='html/text')
        # THEN
        self.assertEqual(self.return_code_200, response.status_code)


if __name__ == '__main__':
    unittest.main()
